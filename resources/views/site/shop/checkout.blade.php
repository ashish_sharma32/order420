@extends('layouts.main') 
@section('title') Checkout - Order420 @stop
@section('content')
<section class="parallax-window"  id="short" >
    <div id="subheader">
        <div id="sub_content">
         <h1>Welcome, @if(Auth::check()){{Auth::user()->firstname}}! @else Guest!@endif Place your order</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="/cart" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>  
        </div> 
        </div>
    </div>
</section>    
<div class="container margin_60_35">
        <div class="row">
            @if(Auth::check() && !is_null($products))
              <div class="col-md-3">
                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    
                    </p>
                </div>
                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box_style_2">
                    <h2 class="inner">Payment methods</h2>
                    <div class="payment_select">
                        <label>Credit card</label>
                        <i class="icon_creditcard"></i>
                    </div>
                    {!! Form::open(array('route'=>'products.postCheckout', 'id' => 'checkout-form','method'=> 'POST')) !!}
                    <div class="form-group">
                        <label>Name on card</label>
                        <input type="text" class="form-control" id="card-name" name="name_card_order" placeholder="First and last name">
                    </div>
                    
                    <div class="form-group">
                        <label>Card number</label>
                        <input type="text" id="card-number" class="form-control" placeholder="Card number">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label>Expiration date</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" id="card-expiry-month" class="form-control" placeholder="mm">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" id="card-expiry-year" class="form-control" placeholder="yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>Security Code</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" id="#card-cvc" class="form-control" placeholder="CCV">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6">
                                        <img src="site/img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Last 3 digits</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                <div id="cart_box">
                    <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                    <table class="table table_summary">
                    <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>
                        <a href="/remove-cart/{{$product['item']->slug}}" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>{{$product['qty']}}x</strong> {{$product['item']->name}}
                        </td>
                        <td>
                            <strong class="pull-right">${{$product['price']}}</strong>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>                    
                    <hr>
                    <div class="row" id="options_2">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                            <label><input type="radio" value="" checked name="option_2" class="icheck">Delivery</label>
                        </div>
                       <!--  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                            <label><input type="radio" value="" name="option_2" class="icheck">Take Away</label>
                        </div> -->
                    </div>
                    <hr>
                    <table class="table table_summary">
                    <tbody>
                    <tr>
                        <td>
                             Subtotal <span class="pull-right">${{$totalPrice}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             Delivery fee <span class="pull-right">$0</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="total">
                             TOTAL <span class="pull-right">${{$totalPrice}}</span>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    <hr>
                    <button type="submit" class="btn_full" href="cart_3.html">Confirm your order</button>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
            @elseif(!Auth::check())
            <div class="col-md-12">
                <div class="box_style_2" id="help">
                    <i class="icon-user"></i>
                    <h4>You Are Not Logged In.</h4>
                    <h3>You need to be registered to proceed further.</h3>
                    <a href="#" data-toggle="modal" data-target="#login" class="phone">Login</a>
                    <hr/>
                    <a href="#" data-toggle="modal" data-target="#register" class="phone">Register</a>
                </div>
            </div>     
            @else
           <div class="col-md-12">
                <div class="box_style_2" id="help">
                    <i class="icon_cart_alt"></i>
                    <h4>Shopping Cart Empty</h4>
                    <a href="/products" class="phone">Go Back To Products Page</a>
                </div>
            </div>    
            @endif
        </div>
</div>
@stop
@section('page-specific-scripts')
{!! Html::script('https://js.stripe.com/v2/') !!}
{!! Html::script('site/js/checkout.js') !!}
@stop