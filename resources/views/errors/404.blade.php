@extends('layouts.main')
@section('title') Page Not Found - Order420 @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>Page Not Found</h1>
            <h1></h1>
            <p>Oops!, The Page You Are Looking For Does Not Exist</p>
            <p></p>
        </div>
    </div>
</section>
</div>
@stop