@extends('layouts.admin')
@section('title') Order Details  - Order420 @stop
@section('page-title')Orders Management @stop
@section('page-content')
<div class="row">
  <div class="col-md-12">
    <div class="invoice">
      <div class="row invoice-header">
        <div class="col-xs-7">
        </div>
        <div class="col-xs-5 invoice-order">
          <span class="invoice-id">Order #{{$order->id}}</span>
          <span class="incoice-date">{{$order->created_at->toDateString()}}</span>
        </div>
      </div>
      <div class="row invoice-data">
        <div class="col-xs-5 invoice-person">
          <span class="name">Ship To</span>
          <span>{{$order->firstname}} {{$order->lastname}}</span>
          <span>{{$order->email}}</span>
          <span>{{$order->address_street}},</span>
          <span>{{$order->address_city}} - {{$order->address_zip}}</span>
          <span>{{$order->country}}</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="invoice-details">
            <tbody>
              <tr>
                <th style="width:60%">Item Description</th>
                <th style="width:17%" class="hours">Quantity</th>
                <th style="width:15%" class="amount">Amount</th>
              </tr>
              @foreach($products as $product)
              <tr>
                <td class="description">{{$product['item']->name}}</td>
                <td class="hours">{{$product['qty']}}</td>
                <td class="amount">${{$product['price']}}</td>
              </tr>
              @endforeach
              <tr>
                <td></td>
                <td class="summary">Subtotal</td>
                <td class="amount">${{$order->totalAmount}}</td>
              <tr>
                <td></td>
                <td class="summary total">Total</td>
                <td class="amount total-value">${{$order->totalAmount}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 invoice-payment-method">
          <span class="title">Payment Method</span>
          <span>Stripe</span>ww
          <span>Payment ID: {{$order->payment_id}}</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 invoice-payment-method">
          <span class="title">Customer Notes</span>
          <span>{{$order->notes}}</span>
        </div>
      </div>
    </div>
  </div>
</div>
@stop