@extends('layouts.main') 
@section('title') Shopping Cart - Order420 @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
         <h1>Welcome, @if(Auth::check()){{Auth::user()->firstname}}! @else Guest!@endif Place your order</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step active">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_2.html" class="bs-wizard-dot"></a>
                </div>
            
                <div class="col-xs-4 bs-wizard-step disabled">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_3.html" class="bs-wizard-dot"></a>
                </div>  
            </div> 
        </div>
    </div>
</section>

<div class="container margin_60_35">
        <div class="row">

            @if(is_null($products))
            <div class="col-md-12">
                <div class="box_style_2" id="help">
                    <i class="icon_cart_alt"></i>
                    <h4>Shopping Cart Empty</h4>
                    <a href="/products" class="phone">Go Back To Products Page</a>
                </div>
            </div>    

            @else
            <div class="col-md-3">
                
                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                </div>
                
                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>
                
            </div>
            
            <div class="col-md-6">
                <div class="box_style_2" id="order_process">
                    <h2 class="inner">Your order details</h2>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                       </div>
                    @endif
                    @if(Auth::check())
                    {!! Form::model(Auth::user(), array('route' => 'orders.storeOrderCustomer')) !!}
                    @else
                    {!! Form::open(array('url' => '/orders/customer')) !!}
                    @endif

                    <div class="form-group">
                        <label>First Name</label>
                        {!! Form::text('firstname',null, array('class' => 'form-control', 'placeholder'=>'First Name', 'required')) !!}  
                    </div>           
                    <div class="form-group">
                        <label>Last Name</label>
                        {!! Form::text('lastname',null, array('class' => 'form-control', 'placeholder'=>'Last Name', 'required')) !!}
                    </div>
                    

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Country Code</label>
                                {!! Form::text('country_code',null, array('class' => 'form-control', 'placeholder'=>'Country Code', 'required')) !!}
                            </div>
                        </div>

                        <div class="col-md-8 col-sm-8">
                            <div class="form-group">
                                <label>Telephone/Mobile</label>
                                {!! Form::text('telephone',null, array('class' => 'form-control', 'placeholder'=>'Telephone/Mobile', 'required')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        {!! Form::email('email',null, array('class' => 'form-control', 'placeholder'=>'Your Email', 'required')) !!}
                    </div>

                    <div class="form-group">
                        <label>Your Full Address and Street</label>
                        {!! Form::text('address_street',null, array('class' => 'form-control', 'placeholder'=>'Address and Street', 'required')) !!}
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>City</label>
                                {!! Form::text('address_city',null, array('class' => 'form-control', 'placeholder'=>'Your City', 'required')) !!}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>State</label>
                                {!! Form::text('address_state',null, array('class' => 'form-control', 'placeholder'=>'Your State', 'required')) !!}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Postal Code</label>
                                {!! Form::text('address_zip',null, array('class' => 'form-control', 'placeholder'=>'Your Zip Code', 'required')) !!}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Country</label>
                                {!! Form::text('country',null, array('class' => 'form-control', 'placeholder'=>'Country', 'required')) !!}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                                <label>Notes for the restaurant</label>
                                <textarea name="notes" class="form-control" style="height:150px" placeholder="" name="notes" id="notes"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                <div id="cart_box">
                    <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                    <table class="table table_summary">
                    <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>
        <a href="/remove-cart/{{$product['item']->slug}}" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>{{$product['qty']}}x</strong> {{$product['item']->name}}
                        </td>
                        <td>
                            <strong class="pull-right">${{$product['price']}}</strong>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    <hr>
                    <div class="row" id="options_2">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                            <label><input type="radio" value="" checked name="option_2" class="icheck">Delivery</label>
                        </div>
                        {{-- <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                            <label><input type="radio" value="" name="option_2" class="icheck">Take Away</label>
                        </div> --}}
                    </div>
                    <hr>
                    <table class="table table_summary">
                    <tbody>
                    <tr>
                        <td>
                            Subtotal <span class="pull-right">${{$totalPrice}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             Delivery fee <span class="pull-right">$0</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="total">
                             TOTAL <span class="pull-right">${{$totalPrice}}</span>
                        </td>
                    </tr>
                    </tbody>
                    </table> 
                    <hr>
                    <button type="submit" class="btn_full">Go to checkout</button>
                    <a class="btn_full_outline" href="/products"><i class="icon-right"></i> Add other items</a>
                {{ Form::close() }}
                </div>
                </div>
            </div>
            @endif
        </div>
</div>
@stop

