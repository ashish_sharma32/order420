Stripe.setPublishableKey('pk_test_uRqbgMv9iDknFGdyGvTZnAzQ');

var $form = $('#checkout-form');

$form.submit(function(event) {
	event.preventDefault();
	$('#charge-error').addClass('hidden');
	Stripe.card.createToken({
	  number: $('#card-number').val(),
	  cvc: $('#card-cvc').val(),
	  exp_month: $('#card-expiry-month').val(),
	  exp_year: $('#card-expiry-year').val(),
	  name: $('#card-name').val()
	}, stripeResponseHandler);
	return false;
});

function stripeResponseHandler(status,response)
{
	if(response.error)
	{
		swal(
  		'Oops...',
  		response.error.message,
  		'error'
		)
		$form.find('button').prop('disabled',false);
	}
	else{
		var token = response.id;
		 $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    	$form.get(0).submit();

	}

}



