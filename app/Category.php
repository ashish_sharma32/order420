<?php

namespace App;

use DB;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['name','slug'];
    
    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }

    public function createRules()
    {
        return $rules = [
        'name' => 'required|min:3|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:categories',
        'slug' => 'unique:categories'
        ];
    }

    public function updateRules($id)
    {
        return $rules = [
        'name' => 'required|min:3|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:categories,name,'.$id,
        'slug' => 'unique:categories,slug,'.$id 
        ];
    }

    public function postCategory() {
        $category = new Category;
        $validator = Validator::make(Request::all(), $this->createRules());
        if($validator->passes())
        {
            $category = new Category;
            $category->create(array('name'=> Request::get('name'),
            'slug'=> str_slug(Request::get('name'), '-')));
        }
        else
        {
            return $validator;
        }
    }

    public function updateCategory($input,$id) {
        $validator = Validator::make($input, $this->updateRules($id));
        if($validator->passes())
        {
            $category = Category::find($id);
            $category -> fill(array('name' => Request::get('name'),
            'slug' => str_slug(Request::get('name'), '-'))) -> save();
        }

        else
        {
            return $validator;
        }
    }
}


