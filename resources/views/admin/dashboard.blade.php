@extends('layouts.admin')
@section('title') Dashboard - Order420 @stop
@section('page-title') Dashboard @stop 
@section('page-content')
<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-4">
        <div class="widget widget-tile">
            <div id="spark1" class="chart sparkline"></div>
            <div class="data-info">
                <div class="desc">Total Users</div>
                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$user_count}}" class="number">0</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4 col-lg-4">
        <div class="widget widget-tile">
            <div id="spark2" class="chart sparkline"></div>
            <div class="data-info">
                <div class="desc">Total Sales</div>
                <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter" data-end="1" data-suffix="%" class="number">0</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4 col-lg-4">
        <div class="widget widget-tile">
            <div id="spark3" class="chart sparkline"></div>
            <div class="data-info">
                <div class="desc">Website Views</div>
                <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter" data-end="532" class="number">0</span>
                </div>
            </div>
        </div>
    </div>
</div>
@stop