@extends('layouts.main') 
@section('title') Home - Order420 @stop
@section('content')
<section class="parallax-window" id="home" data-parallax="scroll" data-image-src="site/img/header.jpg" data-natural-width="1400" data-natural-height="550">
    <div id="subheader">
        <div id="sub_content">
            <h1>Order Cannabis Online</h1>
            {!! Form::open(array('route' => 'products.search' , 'method' => 'get')) !!}
            <div id="custom-search-input">
                <div class="input-group ">                    
                    {!!Form::text('s',null, array('class' => 'search-query' , 'placeholder' => 'Search Products' , 'autocomplete' => 'off' ))!!}
                    <span class="input-group-btn">
                        <input type="submit" class="btn_search" value="submit">
                    </span>
                </div>
            </div>
            {!! Form::close()!!}
        </div>
    </div>
</section>
<div class="container margin_60">
    <div class="main_title">
        <h2 class="nomargin_top" style="padding-top:0">How it works</h2>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="box_home" id="one">
                <span>1</span>
                <h3>Register</h3>
                <p>Yep, That's Pretty Obvious</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box_home" id="two">
                <span>2</span>
                <h3>Search for Products</h3>
                <p>We have more than 1000s products listed.</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box_home" id="three">
                <span>3</span>
                <h3>Pay by card</h3>
                <p>It's quick, easy and totally secure.</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box_home" id="four">
                <span>4</span>
                <h3>Wait For Your Delivery</h3>
                <p>Our Round The Clock Delivery Service Assures Timely Delivery</p>
            </div>
        </div>
    </div>
</div>
<div class="white_bg">
    <div class="container margin_60">
        @if(count($products)>0)
        <div class="main_title">
            <h2 class="nomargin_top">Choose from Most Recent Products</h2>
        </div>
        <div class="row">
            @foreach ($products as $product)
            <div class="col-md-6">
                <a href="/products/{{$product->slug}}" class="strip_list">
                    <div class="desc">
                        <div class="thumb_strip">
                            <img src="{{$product->image}}" alt="">
                          {{--   {{ HTML::image('admin/assets/img/avatar.png', 'Avatar') }} --}}
                        </div>
                       {{--  <div class="rating">
                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                        </div> --}}
                        <h3>{{$product->name}}</h3>
                        <div class="type">
                            {{$product->category->name}}
                        </div>
                        <div>
                            <h4>${{$product->price}}</h4>
                        </div>
                        <ul>
                            <li>Delivery<i class="icon_check_alt2 ok"></i></li>
                        </ul>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        @if($products->count()>=4)
            <a class="btn_full" href="/products">View More</a>
        @endif
        @else
        <div class="main_title">
            <h2 class="nomargin_top">No Products in the Store</h2>
        </div>
        @endif
    </div>
</div>
<div class="high_light">
    <div class="container">
        <h3>Choose from over range of Products</h3>
        <p>Browse our online catalogue on cannabis online dispensary to find the products you are interested in.</p>
        <a href="/products">View all Products</a>
    </div>
</div>
@stop

