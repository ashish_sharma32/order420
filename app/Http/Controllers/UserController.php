<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Mail;
use Request;
use Session;
use Redirect;
use Flash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users')->with(array('users'=>$users,'entity'=>"users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $validator = $user->postUser($request);
        
        if($validator)
        {
            return redirect('/');
        }
        else
        {   
            return redirect('/');
        }
    }

    /**
     * Login User By Authenticating from Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {

        $user = new User;
        $validator = $user->loginUser($request);
        if($validator)
        {
            return redirect()->back()->withInput()->with('errors',$validator->messages())->with('error_code','LV');
        }
        else
        {
            $credentials = [
            'email' => Request::get('email'),
            'password' => Request::get('password'),
            'verified' => 1
            ];

            if(Auth::attempt($credentials))
            {
                if(Auth::user()->admin == true)
                {
                    return redirect()->route('admin.dashboard');
                }
                return redirect()->back();    
            }

            else
            {
                $user = User::where('email', Request::get('email'))->first();
                
                if(!$user){

                return redirect()->back()->withInput()->with('message','Wrong Username/Password Combination')->with('error_code','LV');

                }

                if($user->verified == 0)
                {
                    return redirect()->back()->with('error_code','EmailNotConfirmed');     
                }
                
                return redirect()->back()->withInput()->with('message','Wrong Username/Password Combination')->with('error_code','LV');
            }
        }
    }

    public function confirmMail($token)
    {
        if( ! $token)
        {
            return redirect('/')->with('error_code','NoToken'); 
        }

        $user = User::where('token', $token)->first();

        if ( ! $user)
        {
            return redirect('/')->with('error_code','TokenNotMatched');
        }

        $user->verified = 1;
        $user->token = null;
        $user->save();

        return redirect('/')->with('success_code','EmailConfirmed');
    }
    
     /**
     * Sign Out User.
     */

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');          
    }

    public function checkmail(Request $request)
    {
        $email = Request::get('email');
        if(!Auth::check()){
            if(User::where('email',$email)->exists()){
                return response()->json([
                    "allowed" => "no"
                ]);
            }
        }
        else{
            if(User::where([['email',$email],['id',Auth::user()->id]])->exists()){
                return response()->json([
                    "allowed" => "yes"
                ]);
            }
            else if(User::where('email', $email)->exists()){
                return response()->json([
                    "allowed" => "no"
                ]);
            }
            else{
                return response()->json([
                    "allowed" => "yes"
                ]);
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($user = User::find($id))
        {
            return $user;    
        }                        
        else{
            return "Uses don't exist";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user = new User;
        $input = Request::all();
        $validator = $user->updateUser($id,$input);
        if($validator)
        {
            return "error";    
        }
        else
        {
            return "success"; 
        }
    }
    public function updatePassword(Request $request,$id)
    {
        $user = new User;
        $input = Request::all();
        $validator = $user->updatePassword($id,$input);
        if($validator)
        {
            return "error";
        }
        else
        {
            return "success"; 
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return "success";       
    }
}