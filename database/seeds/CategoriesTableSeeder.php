<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
	    	[
	        'name' => 'Accessories',
	        'slug' => 'accessories'
	    	],
	    	[
	        'name' => 'Concentrates',
	        'slug' => 'concentrates'
	    	],
	    	[
	        'name' => 'Edibles',
	        'slug' => 'edibles'
	    	],
	    	[
	        'name' => 'Flowers',
	        'slug' => 'flowers'
	    	],
	    	[
	        'name' => 'Pre Rolls',
	        'slug' => 'pre-rolls'
	    	]
		]);
    }
}
