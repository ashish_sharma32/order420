@extends('layouts.admin')
@section('title') User Management - Order420 @stop
@section('page-title')User Management @stop
@section('page-content')
<div class="row">
  <div class="col-sm-12">
      <div class="panel panel-default panel-table">
          <div class="panel-heading">
              <p>User Management</p>
          </div>
          <div class="panel-body">
              <table class="table">
                @if(count($users)>0)
                <thead>
                      <tr>
                          <th style="width:50%;">Name</th>
                          <th class="actions">View</th>
                          <th class="actions">Delete</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>{{ $user->firstname}} {{ $user->lastname}}</td>
                        <td class="actions"><a class="icon viewUser" data-id="{{$user->id}}"><i class="mdi mdi-eye"></i></a></td>
                        <td class="actions">
                        {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ,'class' => 'removeForm', 'data-id' => $user->id, 'data-entity' => $entity]) !!}
                        {!! Form::button('Remove', ['class' => 'btn btn-danger rmv','data']) !!}
                        {!! Form::close() !!}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  @else
                  <h3 style="text-align: center">No Users</h3>
                  @endif
            </table>
          </div>
      </div>
  </div>
</div>
<div id="userview" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
  <div class="modal-dialog custom-width">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
        <h3 class="modal-title">User Details</h3>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>First Name</label>
          <input type="text" placeholder="" class="form-control" value="" name="firstname" disabled>
        </div>
        <div class="form-group">
          <label>Last Name</label>
          <input type="text" placeholder="" class="form-control" value="" name="lastname" disabled>
        </div>
        <div class="form-group">
          <label>Email address</label>
          <input type="email" placeholder="" class="form-control" name = "email" disabled>
        </div>
        <div class="form-group">
          <label>Telephone</label>
          <input type="text" placeholder="" class="form-control" name = "telephone" disabled>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-specific-scripts') 
{!! Html::script('admin/assets/js/app.js') !!} 
{!! Html::script('admin/assets/js/rmv_entity.js') !!}
{!! Html::script('admin/assets/js/add_entity.js') !!}
@stop