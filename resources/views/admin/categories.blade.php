@extends('layouts.admin')
@section('title') Categories Management - Order420 @stop 
@section('page-title') Categories Management @stop 
@section('page-content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <p>Categories Management</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <button class="btn btn-info addEntityBtn">ADD NEW CATEGORY</button>
                </div>
            </div>
            <div class="panel-body">
              <table class="table">
              @if(count($categories)>0)
                <thead>
                    <tr>
                        <th style="width:50%;">Category Title</th>
                        <th class="actions">View</th>
                        <th class="actions">Edit</th>
                        <th class="actions">Delete</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($categories as $category)
                  <tr>
                    <td>{{ $category->name}}</td>
                    <td class="actions"><a class="icon viewCategory" data-id="{{$category->id}}"><i class="mdi mdi-eye"></i></a></td>
                    <td class="actions"><a class="icon editCategory" data-id="{{$category->id}}"><i class="mdi mdi-edit"></i></a></td>
                    <td class="actions">
                    {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $category->id] ,'class' => 'removeForm', 'data-id' => $category->id, 'data-entity' => $entity]) !!}
                    {!! Form::button('Remove', ['class' => 'btn btn-danger rmv','data']) !!}
                    {!! Form::close() !!}    
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              @else
                <div class="col-md-12">
                <h3 style="text-align: center">No Category</h3>
                </div>
              @endif
              </table>
            </div>
        </div>
    </div>
</div>

<div id="addEntity" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
  <div class="modal-dialog custom-width">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
        <h3 class="modal-title">Category Details</h3>
      </div>
      <div class="modal-body">
      {!! Form::open(array('route' => 'categories.store','class' => 'popup-form','id'=>'addForm', 'files' => true,'data-entity' => $entity)) !!}
        @if (count($errors) > 0)
            <div class="alert alert-danger error-alert">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
           </div>
        @endif
        <div class="form-group xs-pt-10">
              <label>Category Title</label>
              {!!Form::text('name',null, array('class' => 'form-control','required' , 'placeholder'=> 'Enter Category Title'))!!}
              <div class="alert alert-danger errors">
              </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-space btn-primary addEntitySubmit" >Submit</button>
      {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

<div id="categoryView" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
  <div class="modal-dialog custom-width">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
        <h3 class="modal-title">Category Details</h3>
      </div>
      <div class="modal-body">
      <fieldset disabled> 
        {!! Form::open(array('class' => 'popup-form', 'files' => true)) !!}
          <div class="form-group xs-pt-10">
            <label>ID</label>
            {!!Form::text('id',null, array('class' => 'form-control'))!!}
          </div>
          <div class="form-group xs-pt-10">
            <label>ID</label>
            {!!Form::text('name',null, array('class' => 'form-control'))!!}
          </div>
        {!! Form::close()!!}
      </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div id="categoryEdit" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
  <div class="modal-dialog custom-width">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
        <h3 class="modal-title">Category Details</h3>
      </div>
      <div class="modal-body">
      {{ Form::open(array('method' => 'PUT', 'class' => 'popup-form editForm', 'files' => true )) }}
        @if (count($errors) > 0)
            <div class="alert alert-danger error-alert">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
           </div>
        @endif
        <div class="form-group xs-pt-10">
              <label>Category Title</label>
              {!!Form::text('name',null, array('class' => 'form-control','required' , 'placeholder'=> 'Enter Category Title'))!!}
        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-space btn-primary">Update</button>
      {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@stop
@section('page-specific-scripts')
<script type="text/javascript">
addForm = $("#addForm");
addForm.validate({
rules: {
  name: {
    required: !0,
    minlength: 3
  }
}
});
</script>
{!! Html::script('admin/assets/js/rmv_entity.js') !!}
{!! Html::script('admin/assets/js/add_entity.js') !!}
{!! Html::script('admin/assets/js/view_entity.js') !!}
@stop