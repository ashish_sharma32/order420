<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'firstname' => 'Ashish',
            'lastname' => 'Sharma',
            'email' => 'sharma.asmith7@gmail.com',
            'password' => bcrypt('ASHISH1232'),
            'verified' => '1',
            'telephone' => '8901551232',
            'admin' => '1',
            ],
            [
            'firstname' => 'Steve',
            'lastname' => 'Schaaf',
            'email' => 'compfx@gmail.com',
            'password' => bcrypt('STEVE1232'),
            'verified' => '1',
            'telephone' => '13372803003',
            'admin' => '1',
            ]
        ]);
    }
}