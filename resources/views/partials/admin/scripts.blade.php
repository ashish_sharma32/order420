{!! Html::script('admin/assets/lib/jquery/jquery.min.js') !!}
{!! Html::script('admin/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('admin/assets/js/main.js') !!}
{!! Html::script('admin/assets/js/liquidmetal.js') !!}
{!! Html::script('admin/assets/js/jquery.flexselect.js') !!}
{!! Html::script('admin/assets/lib/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('admin/assets/lib/jquery.sparkline/jquery.sparkline.min.js') !!}
{!! Html::script('admin/assets/lib/countup/countUp.min.js') !!}
{!! Html::script('admin/assets/lib/jquery-ui/jquery-ui.min.js') !!}
{!! Html::script('admin/assets/js/app-dashboard.js') !!}
{!! Html::script('admin/assets/js/sweetalert.min.js') !!}
{!! Html::script('//cdn.tinymce.com/4/tinymce.min.js') !!}
{!! Html::script('admin/assets/js/jquery.serializejson.min.js') !!}
{!! Html::script('https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js') !!}
{!! Html::script('https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js') !!}
<script type="text/javascript">
$(document).ready(function(){
	// initialize the javascript
	App.init();
	App.dashboard(); 
});
</script>
@yield('page-specific-scripts')