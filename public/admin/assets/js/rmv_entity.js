$(".rmv").click(function() {
  $form = $(this).parent(), swal({
    title: "Are You Sure?",
    type: "warning",
    showCancelButton: !0,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No",
    closeOnConfirm: !1
  }, function() {
    $id = $form.data("id");
    $entity = $form.data("entity");
    $.ajax({
      type: "delete",
      url: "/admin/" + $entity + "/" + $id,
        data: {
          _token: $("input[name=_token]").val()
        },
        success: function(t) {
          "error" == t ? swal("Cannot Delete", "Some Errors Occured", "error") : location.reload(!0)
        }
      })
  })
});