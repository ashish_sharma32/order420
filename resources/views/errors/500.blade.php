@extends('layouts.main')
@section('title') Page Not Found - Order420 @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>Internal Server Error</h1>
            <h1></h1>
            <p>Oops!, Some Error Occoured. Please Try Again</p>
            <p></p>
        </div>
    </div>
</section>
</div>
@stop