$(".addEntityBtn").click(function() {
    $("#addEntity").find("form")[0].reset(), $("#addEntity").modal("show")
}), $("#addEntity form").submit(function(t) {
    t.preventDefault();
    if(addForm.valid()){
        $entity = $(this).data('entity');
        var formData = new FormData(this);
        if($entity == 'products'){
            formData.set('description', tinymce.get('descCreate').getContent());
        }    
        $.ajax({
            type: "post",
            url: "/admin/"+$entity,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(t) {
                "success" == t ? location.reload(!0) : swal("Please Try Again", "Some Errors Occured", "error")
            }
        });
    }
});