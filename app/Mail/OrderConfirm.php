<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;


class OrderConfirm extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */

  public $order;

  public function __construct(Order $order)
  {
    $this->order = $order;
  }

  /**
   * Build the message.
   *
   * @return $this
   */

  public function build()
  {
    $cart = unserialize($this->order->cart);
    return $this->subject('[Order420] New Order #'.$this->order->id)->view('emails.orders.confirm')
      ->with([
        'products' => $cart->items,
        'order' => $this->order,
      ]);
  }
}
