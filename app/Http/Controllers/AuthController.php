<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Exception;
use App\User;
use Socialite;

class AuthController extends Controller
{
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        try {
            return Socialite::driver($provider)->redirect();    
        } catch (Exception $e) {
            abort(500, 'The resource you are looking for could not be found');
        }
        
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try{
            $user = Socialite::driver($provider)->user();
            $authUser = $this->findOrCreateUser($user, $provider);
            Auth::login($authUser, true);
            return redirect('/');
        } catch(Exception $e){
            abort(500, 'The resource you are looking for could not be found');
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('email', $user->email)->first();
        
        if ($authUser) {
            return $authUser;
        }
        else{
            $fullname = json_decode($this->split_name($user->name));
            return User::create([
            'firstname'     =>  $fullname->fname,
            'lastname'      =>  $fullname->lname,
            'email'         =>  $user->email,
            'provider'      =>  $provider,
            'provider_id'   =>  $user->id,
            'verified'      =>  1,
            ]);
        }
    }

    public function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        $fullname = array('fname' => $first_name, 'lname' => $last_name);
        return json_encode($fullname);
    }
}
