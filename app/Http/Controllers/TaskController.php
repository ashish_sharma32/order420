<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\User;
use Response;
use Exception;
use App\Task;

class TaskController extends Controller
{
	private $tookanapp_key;
    private $tookanapp_userid;

    public function __construct() {
        $this->tookanapp_key = env('TOOKANAPP_KEY');
        $this->tookanapp_userid = env('TOOKANAPP_USERID');
    }

	public function index(){
		try {
		$client = new Client();
		$body = $client -> request('POST', 'https://api.tookanapp.com/v2/get_all_tasks', [
		'form_params' => [
		'api_key' => $this->tookanapp_key,
		'job_type' => 1
		]
		])->getBody();		
		$tasks = json_decode($body);
		return view('admin.tasks')->with('tasks',$tasks->data);
		} catch (Exception $e) {
		return Response::json('Some Errors Occoured', 503);
		}
	}

	public function show($id,Request $request){
		try {
				$client = new Client();
				$body = $client -> request('POST', 'https://api.tookanapp.com/v2/get_task_details_by_order_id', [
					'form_params' => [
					'api_key' => $this->tookanapp_key,
					'order_id' => $id,
					'user_id' => $this->tookanapp_userid
				]
				])->getBody();
				return $body;
			} catch (Exception $e) {
					return response($e);
			}
	}	
}
