<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Session;
use Mail;
use App\Mail\OrderConfirm;
use Stripe\Charge;
use Stripe\Refund;
use Auth;
use Exception;
use App\Task;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Order extends Model {

    public function task() {
        return $this->hasOne('App\Task');
    }
    public function orderstatus() {
        return $this->belongsTo('App\OrderStatus');
    }    
    public function category() {
        return $this->belongsTo('App\Category');
    }
    protected $fillable=[ 'user_id',
        'cart',
        'totalQty',
        'totalAmount',
        'telephone',
        'email',
        'firstname',
        'lastname',
        'address_street',
        'address_city',
        'address_state',
        'address_zip',
        'country',
        'payment_id',
        'notes'
    ];
    
    public function rules() {
        return $rules=[ 'firstname'=> 'required|min:3|alpha',
        'lastname'=> 'required|min:3|alpha',
        'telephone'=> 'required|between:7,10',
        'country_code'=> 'required',
        'email'=> 'required|email',
        'address_street'=> 'required',
        'address_city'=> 'required',
        'address_state' => 'required',
        'address_zip'=> 'required',
        'country'=>'required',
        ];
    }

    public function putSession($customer) {
        $validator=Validator::make($customer, $this -> rules());
        if ($validator -> passes()) {
            Session::put('customer', $customer);
        }
        else {
            return $validator;
        }
    }

    public function stripeCharge($cart,$token)
    {
        try{
            $charge = Charge::create(array(
            "amount" => $cart->totalPrice * 100,
            "currency" => "usd",
            "description"=> "Test Payment with Refund From User " . Auth:: user()->firstname,
            "source"=> $token,
            ));
            if(!$charge->captured){
                throw new Exception("Charge Not captured");
            }
            else{
                return $charge;
            }
        }
        catch(Exception $e){
            dd($e);
        }
    }

    public function newTask($order) {
        $newOrder = Order::find($order->id);
        $task = new Task;
        $newTask = $task->newTask($order);
        
        if($newTask == 200){
            $newOrder->orderstatus_id = 2;
            Auth::user()->orders()->save($newOrder);
            Session::forget('cart');
            Session::forget('customer');
            return true;
        }
        else{
            $newOrder->orderstatus_id = 3;
            Auth::user()->orders()->save($newOrder);
            Session::forget('cart');
            Session::forget('customer');   
            return false;
        }
    }
    public function newOrder($cart, $token) {
        $charge = $this->stripeCharge($cart,$token);        
        $order = new Order;
        $order->cart=serialize($cart);
        $order->totalQty=$cart->totalQty;
        $order->totalAmount=$cart->totalPrice;
        $order->user_id=Auth::user()->id;
        $order->firstname=Session::get('customer')['firstname'];
        $order->telephone=Session::get('customer')['country_code'].Session::get('customer')['telephone'];
        $order->lastname=Session::get('customer')['lastname'];
        $order->email=Session::get('customer')['email'];
        $order->address_street=Session::get('customer')['address_street'];
        $order->address_city=Session::get('customer')['address_city'];
        $order->address_state=Session::get('customer')['address_state'];
        $order->address_zip=Session::get('customer')['address_zip'];
        $order->country=Session::get('customer')['country'];
        $order->notes=Session::get('customer')['notes'];
        $order->payment_id=$charge->id;
        $order->orderstatus_id = 1;
        Auth::user()->orders()->save($order);
        $ch=Charge::retrieve($charge->id);
        $ch->metadata=array("order_id"=> $order->id);
        $ch->save();
        $this->sendOrderConfirmMail($order->id);
        $newTask = $this->newTask($order);
        if(!$newTask){
            $newOrder = Order::find($order->id);
            $refund = Refund::create(array("charge" => $charge->id)); 
            $newOrder->orderstatus_id = 4;
            Auth::user()->orders()->save($newOrder);
            return false;
        }
        else{
            return $charge;
        }
    }

    public function sendOrderConfirmMail($orderId){
        $order = Order::findOrFail($orderId);
        Mail::to(Session::get('customer')['email'])->send(new OrderConfirm($order));
    }
}