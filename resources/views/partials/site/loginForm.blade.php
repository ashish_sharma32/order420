<!--Login Modal-->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-popup">
                <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
                    {{-- div class="text-left">
                        <a href="#">Forgot Password?</a>
                    </div>
                    <button type="submit" class="btn btn-submit">Submit</button> --}}
                {!! Form::open(array('route' => 'user.login', 'class' => 'popup-form', 'id' => 'myRegister')) !!}
                    <div class="login_icon"><i class="icon_lock_alt"></i></div> 
                    {!!Form::email('email',null, array('class' => 'form-control form-white', 'placeholder'=>'Email', 'required'))!!} 
                    {!!Form::password('password', array('class' => 'form-control form-white', 'id'=>'password1', 'placeholder'=>'Password', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!} 
                    
                    <div><h4 style="color:#fff; padding: 20px;"><strong>Login/Register using below Social Platforms</strong></h4></div>
                    
                    <div class="checkbox-holder text-left col-md-12">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href='/auth/twitter'><i>{{ HTML::image('site/img/twitter.svg', 'Twitter Login', array( 'width' => 37)) }}</i></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href='/auth/facebook'><i>{{ HTML::image('site/img/fb.svg', 'Facebook Login', array( 'width' => 37)) }}</i></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href='/auth/google'><i>{{ HTML::image('site/img/google.svg', 'Google Login', array( 'width' => 37)) }}</i></a>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-submit">Login</button>
                    <br/><br/>
                    @if (Session::has('message'))
                    <div class="alert alert-danger">{{ Session::get('message') }}</div>
                    @endif
                    @if (Session::has('errors'))
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                       </div>
                    @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>