@extends('layouts.main')
@section('title') Order Details - Order420 @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>Order Details</h1>
            <h1></h1>
            <p></p>
        </div>
    </div>
</section>
<div class="container margin_60_35">
    @if(!Auth::check())
    <div class="row">
        <div class="col-md-12">
            <div class="box_style_2" id="help">
                <i class="icon-user"></i>
                <h4>You Are Not Logged In.</h4>
                <h3>You need to be registered to proceed further.</h3>
                <a href="#" data-toggle="modal" data-target="#login" class="phone">Login</a>
                <hr/>
                <a href="#" data-toggle="modal" data-target="#register" class="phone">Register</a>
            </div>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="alert alert-info">
                        <p class="mb-25">Order <strong>#{{$order->id}}</strong> was placed on <strong>{{$order->created_at->toDateString()}}</strong></p>
                    </div>
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                  <th style="width:60%">Product</th>
                  <th style="width:40%">Total</th>
                </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
              <tr>
                <td>{{$product['item']->name}} * {{$product['qty']}}</td>
                <td>${{$product['price']}}</td>
              </tr>
              @endforeach
              <tr>
                <td><strong>SUBTOTAL</strong></td>
                <td>${{$order->totalAmount}}</td>
              </tr>
              <tr>
                <td><strong>TOTAL</strong></td>
                <td>${{$order->totalAmount}}</td>
              </tr>
              </tbody>
            </table>
          </div>
          <h4><u>Shipping Address</u></h4>
          <h5 class="mb-25"> {{$order->firstname}} {{$order->lastname}}
            <br> {{$order->address_street}},
            <br> {{$order->address_city}}, {{$order->address_state}} 
            <br> {{$order->address_zip}}, {{$order->country}}
          </h5>

          <hr/>

          <h4><u>Payment Method</u></h4>
          <h5>Credit/Debit Card</h5> 
          <h5>Payment ID - {{$order->payment_id}}</h5>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@stop