<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;
use App\Mail\UserConfirm;
use Auth;
use Exception;
use Stripe\Charge;
use Stripe\Stripe;
use App\Product;
use App\Category;
use App\Order;


class PageController extends Controller
{

  public function home()
  {
    $products = Product::latest()->paginate(4);
    return view('site.home')->with('products',$products);
  }

  public function about()
  {
  	return view('site.about');
  }

  public function products()
  {
    $products = Product::paginate(4);
    $categories = Category::all();
    return view('site.products')->with(array('products'  => $products ,'categories' => $categories));
  }

  public function showProducts($id)
  {
    if($product = Product::where('slug',$id)->first()){
      return view('site.single-product')->with('product',$product);    
    }
    else{
      abort(404);
    }
  }

  public function myAccount()
  {
    if(Auth::check())
    {
      $orders = Order::where('user_id', Auth::user()->id)->get();
      return view('site.shop.my-account')->with(array('orders' => $orders , 'user' => Auth::user()));
    }
    return view('site.shop.my-account');
  }

  public function dashboard()
  {
    $user_count = User::all()->count();
    return view('admin.dashboard')->with('user_count',$user_count);
  }

  public function testMail(){
    Mail::to('sharma.asmith7@gmail.com')->send(new UserConfirm());
  }
     
}