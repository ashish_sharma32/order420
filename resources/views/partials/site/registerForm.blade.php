<!--Register Modal-->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a> 
            {!! Form::open(array('route' => 'user.store', 'class' => 'popup-form', 'id' => 'myform')) !!}
            <div class="login_icon"><i class="icon_lock_alt"></i></div>
            {!!Form::text('firstname',null, array('class' => 'form-control form-white', 'placeholder'=>'First Name', 'required'))!!}
            {!!Form::text('lastname',null, array('class' => 'form-control form-white', 'placeholder'=>'Last Name', 'required'))!!}
            {!!Form::email('email',null, array('id'=>'email','class' => 'form-control form-white', 'placeholder'=>'Email', 'required'))!!}
            <label id="mail_error" class="error"></label>
            {!!Form::password('password', array('id'=>'password','class' => 'form-control form-white', 'placeholder'=>'Password', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!} 
            {!!Form::password('password_confirmation', array('class' => 'form-control form-white', 'placeholder'=>'Confirm Password', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!} 
            {!!Form::text('telephone',null, array('id'=> 'telephone' ,'class' => 'form-control form-white', 'placeholder'=>'Telephone', 'minlength' => '9' , 'maxlength' => '11' ,'required'))!!}
        <div>
        <h4 style="color:#fff; padding: 20px;"><strong>Login/Register using below Social Platforms</strong></h4>
    </div>

    <div class="checkbox-holder text-left col-md-12">
        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
            <a href='/auth/twitter'><i>{{ HTML::image('site/img/twitter.svg', 'Twitter Login', array( 'width' => 37)) }}</i></a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
            <a href='/auth/facebook'><i>{{ HTML::image('site/img/fb.svg', 'Facebook Login', array( 'width' => 37)) }}</i></a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
            <a href='/auth/google'><i>{{ HTML::image('site/img/google.svg', 'Google Login', array( 'width' => 37)) }}</i></a>
        </div>
    </div>
    <button id="submit" type="submit" class="btn btn-submit">Register</button>
    <br/><br/>
    @if (Session::has('errors'))
    <div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
    {!! Form::close()!!}
    </div>
    </div>
</div>