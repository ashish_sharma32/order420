@extends('layouts.main')
@section('title') My Account - Order420 @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>My Account</h1>
            <h1></h1>
            <p>Manage Your Orders and Personal Details</p>
            <p></p>
        </div>
    </div>
</section>
<div class="container margin_60_35">
    @if(!Auth::check())
    <div class="row">
        <div class="col-md-12">
            <div class="box_style_2" id="help">
                <i class="icon-user"></i>
                <h4>You Are Not Logged In.</h4>
                <h3>You need to be registered to proceed further.</h3>
                <a href="#" data-toggle="modal" data-target="#login" class="phone">Login</a>
                <hr/>
                <a href="#" data-toggle="modal" data-target="#register" class="phone">Register</a>
            </div>
        </div>
    </div>
    @else
    <div class="main_title">
        <h2 class="nomargin_top" style="padding-top:0">Welcome @if(Auth::check()){{Auth::user()->firstname}}@endif</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#orders" data-toggle="tab" aria-expanded="false" class="orderLink">My Orders</a></li>
                <li class=""><a href="#profile" data-toggle="tab" aria-expanded="true" class="profileLink">Profile</a></li>
                <li class=""><a href="#password" data-toggle="tab" aria-expanded="true" class="profileLink">Update Password</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="orders">
                    @if(!$orders->isEmpty())
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Ship To</th>
                                    <th>Customer Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td><a href="/order-details/{{$order->id}}" class="btn btn-primary">{{$order->id}}</a></td>
                                    <td>{{$order->address_street}}, {{$order->address_city}} - {{$order->address_zip}} <br/> {{$order->country}}</td>
                                    <td>{{$order->firstname}} {{$order->lastname}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <h3 style="text-align: center">No Orders</h3>
                    @endif
                </div>
                <div class="tab-pane" id="profile">
                {!! Form::model($user, ['id' => 'updateUser', 'method' => 'PUT','route' => ['user.update', $user->id]])!!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                          </ul> 
                       </div>
                    @endif
                    <div class="form-group">
                        <label>First name</label>
                        {{Form::text('firstname',null,['class' => 'form-control form-white form-account'])}}
                    </div>
                    <div class="form-group">
                        <label>Last name</label>
                        {{Form::text('lastname',null,['class' => 'form-control form-white form-account'])}}
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        {{Form::email('email',null,['id'=>'email','class' => 'form-control form-white form-account'])}}
                        <label id="mail_error" class="error"></label>
                    </div>
                    <div class="form-group">
                        <label>Telephone</label>
                        {{Form::tel('telephone',null,['class' => 'form-control form-white form-account'])}}
                    </div>
                    <div class="form-group">
                        <label>Address and Street</label>
                        {{Form::text('address_street',null,['class' => 'form-control form-white form-account'])}}
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>City</label>
                                {{Form::text('address_city',null,['class' => 'form-control form-white form-account'])}}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Postal Code</label>
                                {{Form::text('address_zip',null,['class' => 'form-control form-white form-account'])}}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Country</label>
                                {{Form::text('country',null,['class' => 'form-control form-white form-account'])}}
                            </div>
                        </div>
                    </div>

                    <button type="submit" id="submit" class="btn btn-submit">Update</button>
                {{Form::close()}}
                </div>
                <div class="tab-pane" id="password">
                {{ Form::open(['id' => 'updatePass', 'method' => 'PUT','route' => ['user.updatepass', $user->id]]) }}
                @if (count($errors) > 0)
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>  
                            @endforeach
                          </ul>
                       </div>
                @endif
                <div class="form-group">
                    <label>Enter New Password</label>
                    {!!Form::password('password', array('id'=>'pwd','class' => 'form-control form-white form-account', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!} 

                </div>
                <div class="form-group">
                    <label>Re-Enter Password</label>
                    {!!Form::password('password_confirmation', array('class' => 'form-control form-white form-account', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!} 
                </div>
                <button type="submit" id="submit" class="btn btn-submit">Update</button>
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@stop
@section('page-specific-scripts')
@if(Auth::check())
<script type="text/javascript">
$(function() {
    function e(e) {
        "success" == e ? swal("Congrats", "Your Details have been Successfully Updated", "success") : swal("Oops...", "Some Errors Occurred. Please Try Again", "error"), $("button#submit").html("Update")
    }

    function a() {
        $spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>", $("button#submit").html($spinner)
    }
    $(document).on("click", ".getOrder", function(e) {
        $id = $(this).data("id"), $.ajax({
            type: "get",
            url: "/getorder/" + $id,
            success: function(e) {
                console.log(e)
            }
        })
    }), updateUser = $("#updateUser"), updatePass = $("#updatePass"), updateUser.validate({
        rules: {
            firstname: {
                required: !0,
                minlength: 3
            },
            lastname: {
                required: !0,
                minlength: 3
            },
            email: {
                required: !0
            },
            telephone: {
                required: !0
            },
            address_street: {
                required: !0
            },
            address_city: {
                required: !0
            },
            address_zip: {
                required: !0
            },
            country: {
                required: !0
            }

        },
        submitHandler: function(n) {
            a();
            var s = {
                firstname: $("input[name=firstname]").val(),
                lastname: $("input[name=lastname]").val(),
                email: $("input[name=email]").val(),
                telephone: $("input[name=telephone]").val(),
                address_street: $("input[name=address_street]").val(),
                address_city: $("input[name=address_city]").val(),
                address_zip: $("input[name=address_zip]").val(),
                country: $("input[name=country]").val(),
                _token: $("input[name=_token]").val()
            };
            $.ajax({
                type: "put",
                url: "user/{{$user->id}}",
                data: s,
                success: function(a) {
                    e(a);
                }
            })
        }
    }), updatePass.validate({
        rules: {
            password: {
                minlength: 8,
                alphanumeric: !0
            },
            password_confirmation: {
                minlength: 8,
                alphanumeric: !0,
                equalTo: "#pwd"
            }
        },
        submitHandler: function(n) {
            a();
            var s = {
                password: $("input[name=password]").val(),
                password_confirmation: $("input[name=password_confirmation]").val(),
                _token: $("input[name=_token]").val()
            };
            $.ajax({
                type: "put",
                url: "/updatepass/{{$user->id}}",
                data: s,
                success: function(a) {
                    e(a)
                }
            });
        }
    })
});
</script>
@endif
@stop