<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use Auth;
use Exception;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Client;

class Task extends Model
{
	private $tookanapp_key;
    private $tookanapp_userid;

    public function __construct() {
        $this->tookanapp_key = env('TOOKANAPP_KEY');
        $this->tookanapp_userid = env('TOOKANAPP_USERID');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

	public function newTask($order) { 
		$client = new Client();
		try {
			$current = Carbon::now();
			$order_delivery_datetime = $current->addDays(2)->toDateTimeString();
    		$task = $client->request('POST', 'https://api.tookanapp.com/v2/create_task', [
			'form_params' => [
		    'api_key' => $this->tookanapp_key,
		    'order_id' => $order->id,
		    'job_description' => 'Delivery Order #'.$order->id,
		    'customer_email' => Session::get('customer')['email'],
		    'customer_username' => Session::get('customer')['firstname'] . ' ' . Session::get('customer')['lastname'],
		    'customer_phone' => Session::get('customer')['country_code'] . Session::get('customer')['telephone'],
			'customer_address'=> Session::get('customer')['address_street'].', '.Session::get('customer')['address_city'].', '.Session::get('customer')['address_state'].', '. Session::get('customer')['address_zip'].', '.Session::get('customer')['country'],
		    'job_delivery_datetime' => $order_delivery_datetime,
		    'custom_field_template' => 'cannabis_delivery',
		    'auto_assignment' => '1',
		    'has_pickup' => '0',
		    'has_delivery'=> '1',
		    'layout_type' => '0',
		    'tracking_link' => 1,
		    'timezone' => '+700',
		    'ref_images'=> ['http://order420.com/uploads/products/test.jpg'],
		    'notify' => 1,
		    'geofence' => 1,
		  ]])->getBody();
    	$body = json_decode($task);
    	return $body->status;   
  	} catch (Exception $e) {
    	return null;
    }       
  }
}