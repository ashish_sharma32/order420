{!! Html::script('site/js/jquery-1.11.2.min.js') !!}
{!! Html::script('site/js/common_scripts_min.js') !!}
{!! Html::script('site/js/functions.js') !!}
{!! Html::script('https://cdn.jsdelivr.net/sweetalert2/6.3.0/sweetalert2.min.js') !!}
{!! Html::script('https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js') !!}
{!! Html::script('https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js') !!}
{!! Html::script('site/js/alertify.js') !!}
{!! Html::script('site/js/app.js') !!}
@yield('page-specific-scripts')
@if (Session::get('error_code')=='NoToken')
<script>
swal(
  'Oops...',
  'Something went wrong!',
  'No Token is Passed For Email Verification'
)
</script>
@endif

@if (Session::get('error_code')=='TokenNotMatched')
<script>
swal(
  'Oops...',
  'User with this Token Not Found',
  'error'
)
</script>

@endif

@if (Session::get('success_code')=='EmailConfirmed')
<script>
swal(
  'Congrats',
  'Your E-Mail has been Verified',
  'success'
)
</script>
@endif

@if (Session::get('error_code')=='EmailNotConfirmed')
<script>
swal(
  'Oops...',
  'Your E-Mail has not been verified yet. Please Check your Mail.',
  'error'
);
</script>

@endif

@if (Session::get('success_code')=='AccountCreated')
<script>
 alertify.success("Your Account Has Been Succesfully Created");
</script>
@endif


@if (Session::get('success_code')=='UserUpdated')
<script>
$('a.profileLink').click();
swal(
  'Success',
  'Your Account Details has been updated.',
  'success'
)
</script>
@endif

@if (Session::get('error_code')=='UUE')
<script>
$('a.profileLink').click();
</script>
@endif


@if (Session::get('error_code')=='NoToken')
<script>
swal(
  'Oops...',
  'No Token is Passed For Email Verification',
  'error'
)
</script>

@endif


@if (Session::get('error_code') && Session::get('error_code')=='RV')
<script>
$(function() {
    $('#register').modal('show');
});
</script>
@endif



@if (Session::get('error_code') && Session::get('error_code')=='LC' || Session::get('error_code')=='LV' )
<script>
$(function() {
    $('#login').modal('show');
});
</script>

@endif	

@if (Session::get('popup') && Session::get('popup')=='register')
<script>
$(function() {
    $('#register').modal('show');
});
</script>
@endif	

@if (Session::get('error_code')=='NotAuth')
<script>
swal(
  'Oops...',
  'Only Admin Can Access This Page',
  'error'
)
</script>
@endif

<script type="text/javascript">

$(document).ready(function() {
 $.ajax({
      type: "get",
      url: "/get-cart",
      success: function(e) {
      if(e=='Empty Cart')
      {
      $(".cart_table").hide();
      $(".cartItems p").text("Empty Cart");
      }
      else{
        $(".cart_table").show();
        $(".cartItems p").hide();
        $.each(e.cart, function(index, element) {
          $rowopen = "<tr>";
          $rowd1 = "<td><strong class='cart-widget-qty'>"+element.qty+" x </strong><span class='cart-widget-name'>"+element.item.name+"</span></td>";
          $rowd2 = "<td>$"+element.price+"</td>";
          $rowclose = "<tr/>";
          $('.cart-widget').append($rowopen+$rowd1+$rowd2+$rowclose);
        });
      }
    }
  });

 $('.addCart').on('click', function(event) {
    $this = $(this);
    $slug = $(this).data("slug");
    $.ajax({
        type: "get",
        url: "/add-to-cart/" + $slug,
        success: function(e) {
            swal(
            'Success',
            'Product Added To Cart',
            'success'
            );
            $(".cart-widget").empty();
            $(".cartItems p").hide();
            $(".cart_table").show();
            $.ajax({
            type: "get",
            url: "/get-cart",
            success: function(e) {
            $(".cartCount").text(e.totalQty); 
            $.each(e.cart, function(index,element) {
              $rowopen = "<tr>";
              $rowd1 = "<td><strong class='cart-widget-qty'>"+element.qty+" x </strong><span class='cart-widget-name'>"+element.item.name+"</span></td>";
              $rowd2 = "<td>"+element.price+"</td>";
              $rowclose = "</tr>";
              $('.cart-widget').append($rowopen+$rowd1+$rowd2+$rowclose);
            });
            } 
        });
      }
    });
});
});
</script>