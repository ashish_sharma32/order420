@extends('layouts.main')
@section('title') {{$product->name}} - Order420 @stop
@section('content')
<!--Content-->
<section class="parallax-window">
    <div id="subheader">
        <div id="sub_content">
            <h1>{{$product->name}}</h1>
            <div><em>{{$product->category->name}}</em></div>
        </div>
    </div>
</section>

<div class="container margin_60_35">
    <div class="row">
        <div class="col-md-4">
            <div class="box_style_2" id="help">
                <img class="img-responsive" src="{{$product->image}}" alt="Chania" align="center">
            </div>
        </div>
        <div class="col-md-8">
            <div class="box_style_2">
                <h2 class="inner">Description</h2>
                <h3>{{$product->name}}</h3>
                <h5><a href="/search?s={{$product->category->slug}}">{{$product->category->name}}</a></h5>
              <!--   <h5><a href="/search?s={{$product->category->slug}}"{{$product->category->name}}</a></h5> -->

                <p class="add_bottom_30">{!! $product->description !!}</p>
                <div id="summary_review">
                    <div id="general_rating">
                        <span>${!! $product->price !!}</span>
                        <a class="addCart btn_1 add_bottom_15" data-slug="{{$product->slug}}">Add To Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-specific-scripts')
<script type="text/javascript">
</script>
@stop