// /*Bind EnterKey with Delete Button*/
// $("button.confirm").keypress(function(e){
//     if (e.which == 13){
//         $(this).click();
//     }
// });

// /*View User*/
// $firstname = $("input[name ='firstname']"); 
// $lastname = $("input[name ='lastname']");
// $email = $("input[name ='email']"); 
// $telephone = $("input[name ='telephone']"); 

// $(".viewUser").click(function() {
//     $id = $(this).data("id");
//     $.ajax({
//         type: "get",
//         url: "/admin/users/" + $id,
//         success: function(e) {
//             $user = e, $id = $user.id, $fname = $user.firstname, $lname = $user.lastname, $em = $user.email, $tel = $user.telephone, $firstname.val($fname), $lastname.val($lname), $email.val($em), $telephone.val($tel)
//         }
//     }), 
//     $("#userview").modal("show");
// }); 


// $name = $("input[name ='name']");
// $amount = $("input[name ='price']");
// $image = $("img#productImage");

// /*Create Product*/
// $(".createProduct").on("click", function(event) {
//     event.preventDefault();
//     $name.val("");
//     $amount.val("");
//     $image.attr("src", "");
//     tinymce.get("descCreate").setContent("");
//     $('option[value="1"]').attr("selected", "selected");
//     $("#productCreate").modal("show");
// });


// /*View Product*/
// $(".viewProduct").click(function() {
//     $id = $(this).data("id");
//     $.ajax({
//         type: "get",
//         url: "/admin/products/" + $id,
//         success: function(e) {
//             $product = e;
//             $pname = $product.name;
//             $pdesc = $product.description;
//             $pamount = $product.price;
//             $pcategory = $product.category_id;
//             $pimage = $product.image;
//             $('option[value="' + $pcategory + '"]').attr("selected", "selected");
//             $name.val($pname);
//             tinymce.get("descView").setContent($pdesc);
//             $amount.val($pamount);
//             $image.attr("src", $pimage);
//             $("#productView").modal("show");
//         }
//     });
// });

// /*Edit Product*/

// $(".editProduct").click(function() {
    
//     $id = $(this).data("id");
//     $url = "/admin/products/" + $id;
//     $(".editForm").attr('action', $url);
//     $.ajax({
//         type: "get",
//         url: $url,
//         success: function(data) {
//             $product = data;
//             $pname = $product.name;
//             $pdesc = $product.description;
//             $pamount = $product.price;
//             $pcategory = $product.category_id;
//             $pimage = $product.image;
//             $('option[value="' + $pcategory + '"]').attr("selected", "selected");
//             $name.val($pname);
//             tinymce.get("descEdit").setContent($pdesc);
//             $amount.val($pamount);
//             $image.attr("src", $pimage);
//             $("#productEdit").modal("show");  
//         }
//     });
// });

// /*View Category*/
// $idElem = $("input[name ='id']");
// $name = $("input[name ='name']");

// $(".viewCategory").click(function() {
//     $id = $(this).data("id");

//     $.ajax({
//         type: "get",
//         url: "/admin/categories/" + $id,
//         success: function(e) {
//             $category = e;
//             $cid = $category.id;
//             $cname = $category.name;
//             $idElem.val($cid);
//             $name.val($cname);
//             $("#categoryView").modal("show");
//         }
//     });
// });

// /*Edit Category*/
// $(".editCategory").click(function() {
//     $url = "/admin/categories/" + $(this).data("id");
//     $(".editForm").attr('action', $url);
    
//     $.ajax({
//         type: "get",
//         url: $url,
//         success: function(e) {
//             $category = e;
//             $cname = $category.name;
//             $name.val($cname);
//             $("#categoryEdit").modal("show");  
//         }
//     });
// });
// /*View Task*/
// $(".viewTask").click(function() {
//     $id = $(this).data("id");
//     $.ajax({
//         type: "get",
//         url: "/admin/tasks/" + $id,
//         success: function(e) {
//             $result = JSON.parse(e);
//             $result = $result.data[0];
//             console.log($result);
//             var taskStatus =
//             [{value : "0" , desc : "The task has been assigned to a agent."},
//              {value : "1" , desc : "The task has been started and the agent is on the way."},
//              {value : "2" , desc : "The task has been completed successfully."},
//              {value : "3" , desc : "The task has been completed unsuccessfully."},
//              {value : "4" , desc : "The task is being performed and the agent has reached the destination."},
//              {value : "6" , desc : "The task has not been assigned to any agent."},
//              {value : "7" , desc : "The task has been accepted by the agent which is assigned to him."},
//              {value : "8" , desc : "The task has been declined by the agent which is assigned to him."},
//              {value : "9" , desc : "The task has been cancelled by the agent which is accepted by him."},
//              {value : "10" , desc : "The task is deleted from the Dashboard."}
//             ];
//             $fetchTaskDesc= $.grep(taskStatus, function(e){return e.value == $result.job_status;});
//             $job_state_desc = $fetchTaskDesc[0].desc;
//             $(".job_state_desc").html($job_state_desc);
//             $(".job_state").val($result.job_state);
//             $(".task-title").val($result.job_id);
//             $(".job_delivery_datetime").val($result.job_delivery_datetime);
//             $(".customer_username").val($result.customer_username);
//             $(".customer_phone").val($result.customer_phone);
//             $(".customer_email").val($result.customer_email);
//             $(".job_address").val($result.job_address); 
//             $("#taskView").modal("show");
//         }
//     });
// });