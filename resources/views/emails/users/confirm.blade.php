@extends('emails.layout')
@section('page-content')
<tr>
  <td bgcolor="#ffffff" align="center" style="padding: 15px;">
    <!--[if (gte mso 9)|(IE)]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
      <tr>
        <td align="center" valign="top" width="500">
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Thanks For Registering!</td>
                  </tr>
                  <tr>
                    <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">Please Click The Button Below to Veify You E-Mail Address</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
        </td>
      </tr>
    </table>
    <![endif]-->
  </td>
</tr>
      <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 15px;">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" style="padding-top: 25px;" class="padding">
                            <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                              <tr>
                                <td align="center" style="border-radius: 3px;" bgcolor="#256F9C"><a href="{{url('/')}}/register/verify/{!! $user->token !!}" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; border: 1px solid #256F9C; display: inline-block;" class="mobile-button" data-saferedirecturl="{{url('/')}}/register/verify/{!! $user->token !!}">Confirm</a></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
        </td>
      </tr>
    </table>
    <![endif]-->
    </td>
    </tr>
@stop