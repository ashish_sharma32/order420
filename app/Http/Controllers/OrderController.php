<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Request;
use Session;
use Auth;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('admin.orders')->with('orders',$orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($order = Order::find($id))
        {
            $cart = unserialize($order->cart);
            return view('admin.order_view')->with(array('products'  => $cart->items ,'order' => $order));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function storeOrderCustomer(Request $request)
    {
        if(Auth::check()){
            $this->updateUserWithCart();
            $customer = Request::all();
            $order = new Order;
            $validator = $order->putSession($customer);
            if($validator)
            {
                return redirect()->back()->with('errors',$validator->messages());
            }
            return redirect('/checkout');  
        }
        else{
            return redirect('/checkout');
        }
    }

    public function updateUserWithCart(){
        $id = Auth::user()->id;
        $user = new User;
        $input = Request::all();
        $user->updateUser($id,$input);
    }

    public function orderDetails($id){
    if($order = Order::find($id)){
          if($order->user_id == Auth::user()->id){
            $cart = unserialize($order->cart);
            return view('site.shop.order-details')->with(array('products'  => $cart->items ,'order' => $order));
          }
          else{
            abort(404);
          }
        }
        else{
           abort(404);
        }   
    }
}
